# Deploy a new Escriptorium instance

We will see here how how to deploy a new escriptorium instance when we only have a server or more with ssh access. The full deployement has only been ran and tested on Ubuntu (20.04 and 22.04).

For the example, we will use 2 servers `master1` and `worker1`.
If you only have a one server setup, you'll just need a different configuration in `docker:` section. (see below)

First, has you'll use this project, clone this repo and link it into `~/.ansible/roles`, then it will be accessible from your personnal repowhen using Ansible.

## Setup ansible project

On a new personnal git repo (or personnal computer directory) like `my_escriptorium_setup`:

Create a `hosts` file with your servers hostnames in a group, here `intance-prod` :
```
[instance-prod]
master1
worker1
```

Create directories `group_vars/` and `host_vars/`. This is where all configurations will be written for ansible roles.
You need three files, ordered like this :
```
group_vars/
 |--> instance-prod  # File for the group configuration, applied to all hosts in group
 |
host_vars/  # Specific configuration for each hosts
 |--> master1
 |--> worker1
```

Then create a `playbook.yml` which will be your entrypoint for launching Ansible roles, basic setup example :
```
# You can either specify group names of host names
- hosts:
    - instance-prod
    - master1
    - worker1

  roles:
    - role: escriptorium
      tags: escriptorium

```

## Setup Ansible vault for secret management

For secret management in Ansible, we use internal ansible vault. It uses a password for cipher and decipher texts. This allow you to keep passwords in your public configuration without them beeing accessible to non authorized people.
More information about ansible-vault with `ansible-vault --help`

### Create vault-id file : password.gpg

In order to allow admin people to decipher your secrets you need a master password. In order to do it we encrypt the master password as a `password.gpg` file that can only be deciphered by admins. Here are the steps to follow :

- Create a file named `password` with the master password written in it.
- The public gpg key from admins that will have access. (gpg keys can be generated with `gpg --full-generate-key` then `gpg --armor --export <key_id> --output <output_file>.pub` to export it. More informations here: https://www.redhat.com/sysadmin/creating-gpg-keypairs)

- Import the public keys to your list of keys `gpg --import <pub_key>.pub`
- You can list all your keys with `gpg --list-all-keys`
- Create a the `password.gpg` with all administrators keys as recipient `gpg --encrypt --recipient user1@admin.com --recipient user2@admin.com --output password.gpg password`
- Make sure the file `password` has been replaced and does not exist anymore
- Commit and push your changes.

Now this new file `password.gpg` can be deciphered by all admins with their own passwords.

### Script for ansible-vault decipher

When you have your `password.gpg`, you need to create a shell script that will be used by ansible-vault when you run playbooks and roles.

```
#!/bin/sh
# Script used by ansible-vault
gpg --quiet --decrypt ./password.gpg
```

As you'll see later, you can now run roles with the option `--vault-id=./password` and all encrypted passwords will be accessible for deployment and only for admins allowed to run it.

## Host and application configuration

Here is a sample configuration file for a host :
```
# See Ansible configuration for ssh access to you host (depends on vpn, ssh challenges etc...)
ansible_ssh_host: <ssh_host>

# Needs to be true to execute commands as root
ansible_become: true

hostname: <hostname>
vlan_ip: <internal_ip_for_swarm_communications>

# Needed for swarm to know which node is manager
docker:
  enabled: true
  data: /var/lib/docker

  swarm:
    enabled: true # false if host is single and no swarm is needed
    manager: true # false if node is worker
    network_services: "{{ _network_services }}" # Give a name for network name used by docker swarm services, set in groups_var file, see below

  # Labels are usefull for service balancing, labels are "app", "worker" or "frontend"
  swarm_labels:
    - escriptorium_mode: app
    - escriptorium_mail: "yes" # needed if you want your mail service to run on specific host

# Traefik is a loadbalancer which will listen to public traffic and redirect to you escriptorium website
traefik:
  enabled: true
  version: "2.10"

  networks:
    - "{{ _network_services }}"

  # Will configure certificate automatic generation
  acme:
    email: contact@me.com
    domains:
      - main: "my.escriptorium.com"
        sans: [""] # you can configure DNS alias here

  # DNS challenger, either 'tls' or 'http'
  challenge: http
```

All configurations for escriptorium are stored in the group file `groups_var/instance-prod`. If you are using a single host you can also add it to the end of your host configuration instead.
```
# Shared network for swarm (used in host_vars)
_network_services: "services"

# General hostname
public_hostname: my.escriptorium.com

escriptorium:
  enabled: true
  name: prod  # instance name, prefix for swarm services names
  use_www: false  # Use www. prefix for the public_hostname
  version: v0.13.8
  dir: /home/prod  # Home directory for escriptorium
  data:
    dir: "/data/prod" # Data directory, need to be shared with all nodes

    # Set to false if you don't need to mount nfs dir with docker local driver
    nfs:
      local_driver: true
      device: ":/mnt/datapool0/public"
      options: "addr=mynfs.com,nolock,soft,rw"
      target: "/data"
      volume_name: esc_prod_nfs

  public_hostname: my.escriptorium.com

  # Postgres version
  db_version: 15.2
  redis_image: "redis:6"
  email_host: my.mail.fr

  # Configuration for elasticsearch
  search:
    enabled: yes
    run_auto_indexation: true
    es_version: 7.17.8
    es_placements:
      - "node.labels.escriptorium_mode == app"

  # Allwo text_alignement
  text_alignment_enabled: true

  # Set custom CSP for nginx
  nginx_header_content_security_policy: "default-src 'self' img-src 'self'; script-src 'self' 'unsafe-eval' 'self'; style-src 'self'"

  # Root password for postgres
  db_root_password: tYKNiB16hvVJk0OaMNnYV1uqXYYVPTTGevHIXv0MoR

  django_secret_key: c228R0LCzxgWVIE9FYQqmwX1RzyEYYajLTPnooRdz0

  django_admin_password: gX6atkIYahwdDjad7hTQgngVpZNVV97T3WLmlb5kBd

  # List of workers to deploy, this is an example, you can have more or less workers depending on your setup
  workers:
    - name: main
      cpus: 2  # CPU used by worker
      queues:  # assign worker to queues (default, live, low-priority, jvm and gpu) best to have at least 1 worker per queue)
        - default
      constraints:
        - node.labels.escriptorium_mode == worker  # Label to assign worker to specific host (not needed if not using swarm)
    - name: live
      cpus: 2
      queues:
        - live
      constraints:
        - node.labels.escriptorium_mode == worker
    - name: low-priority
      cpus: 1
      queues:
        - low-priority
      constraints:
        - node.labels.escriptorium_mail == yes
    - name: jvm
      cpus: 2
      queues:
        - jvm
      constraints:
        - node.labels.escriptorium_mode == worker
      max_tasks_per_child: 1
      # Override worker env variable
      env:
        _JAVA_OPTIONS: "-Xms518m -Xmx518m"

    - name: gpu-0
      cpus: 2
      queues:
        - gpu
      constraints:
        - node.labels.escriptorium_mode == worker
      env:
        # These two environment variables control which GPU are enabled on the worker
        NVIDIA_DRIVER_CAPABILITIES: "all"
        NVIDIA_VISIBLE_DEVICES: "<GPU_UUID>"
        # Specify which GPU kraken can use
        KRAKEN_TRAINING_DEVICE: "cuda:0"
      mounts:
        # Simulate shm_size: 8Gb
        # This is needed to run training through pytorch
        - type: tmpfs
          target: /dev/shm
          tmpfs_size: 8G

  # Specify where to deploy postgres and nginx frontend
  db_placements:
    - "node.labels.escriptorium_mode == <label>"
  frontend_placements:
    - "node.labels.escriptorium_mode == <label>"
```

All label used in configuration must be declared in `host_vars/` files, default is `app`

## Makefile

To ease deployment, you should write a `Makefile` like this one :

```
ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

extras="-t stack"

instance-prod:
	ansible-playbook playbook.yml -i hosts --vault-id=./password --limit=instance-prod $(extras)
```

Now you can deploy the escriptorium app with `make instance-prod` or you can deply specific tags or use ansible options using extras variables : `make instance-prod extras="-t docker --limit=master1"`

## Deploy the new instance

First, you want to install and setup docker. Whether or not you are using docker swarm, use : 

`make instance-prod extras="-t docker"

Now your servers are setup, check docker swarm is healthy with `docker node ls` on manager node.
You just need to deploy escriptorium, again, it is the same command deploying on a swarm setup or not :

`make instance-prod`

When all services or containers are running, you just need to deploy traefik to gain web access :

`make instance-prod extras="-t traefik`

You've just finished deploying you escriptorium stack ! You can connect on https://my.escriptorium.com/
