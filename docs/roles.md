# Roles

## Tasks description

The `escriptorium-deployment` project is only one Ansible role allowing you to setup and deploy an escriptorium instance running on docker or docker swarm.

Here is the detail or the role tasks:

- `main.yml` is the entrypoint for the role, it will route the playbook through all other tasks depending on configuration and tags used.
- `docker.yml` is the task for full docker setup, whether you use docker swarm or not. After this task, docker should be up and running.
- `swarm.yml` will setup swarm configuration (create master node and join workers). It is called by `docker.yml` if needed.
- `swarm_labels.yml` setup hosts labels in swarm configuration, usefull to label web, gpu or worker node for example.
- `traefik.yml` setup and deploy traefik which is the loadbalancer allowing clients to reach escriptorium website.
- `stack.yml` setup all variables and create directories for the escriptorium whole app deployment.
- `containers.yml` actually deploys the escriptorium app using docker containers. Called by `stack.yml` when docker swarm is not configured.
- `services.yml` actually deploys escriptorium app using swarm services. Called by `stack.yml` when using docker swarm cluster setup.
- `flower.yml` deploys flower monitoring tool for celery workers. Deploys container or swarm service depending on your setup.


## Available tags

By default, `make cremma-prod` is configured to use `stack` tags which will update/deploy escriptorium stack, but you can use many other tags for specific deployments.
In order to specify the tags ou want to run you need to add an option `make cremma-prod extras="-t <tag1>,<tag2>"` with as many tags you want.

### Overall setup tags

Those tags should mostly be used when configuring a new host.

- `docker`: Install docker, setup docker configuration, setup swarm cluster, add swarm labels to hosts and restart docker when changes occured.
- `labels`: Only add swarm labels to hosts, may be usefull when changing labels or making sure labels are correctly applied.
- `traefik`: Only deploy traefik which is the loadbalancer between clients and escriptorium application.

### Application tags

All application tags can be used either to deploy a new instance or to update/upgrade a running one. Ansible will only execute tasks when the new parameters specified in the configuration are different than the one deployed on host.

- `stack`: This is the default one, it will setup all variables and deploy escriptorium depending on the stack type you have configured (swarm services or single hosted docker containers).
- `app`: Only deploy escriptorium application services/containers (app and channel server). Little downtime as the application will need to restart.
- `migrate`: Only run web app migration, like database migration and collect static files.
- `workers`: Only deploy escriptorium workers without restarting web app. No downtime.
- `nginx`: Only deploy nginx service/container. It is the one application between traefik and escriptorium app.
- `flower`: Only deploy flower celery worker monitoring service.
- `redis`: Only deploy redis service/container.
- `elasticsearch`: Only deploy elasticsearch service/container used for search features.
- `backups`: Create backup folders and backup scripts for escriptorium database and media files backups.
