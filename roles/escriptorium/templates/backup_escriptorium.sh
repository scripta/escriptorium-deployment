#!/bin/bash -e
# {{ ansible_managed }}

# Dump database
docker exec -i $(docker ps -q -f name={{ escriptorium.name }}-db) pg_dump -U postgres escriptorium > {{ escriptorium.name }}.sql
