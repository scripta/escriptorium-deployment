upstream escriptorium {
    server {{ escriptorium.name }}-web:8000;
    # server unix:/usr/src/app/escriptorium/app.sock;
}

upstream websocket {
    server {{escriptorium.name }}-channels:5000;
}


server {
    listen 8080;
    charset     utf-8;
    client_max_body_size 1000000M;

    gzip_static  on;
    gzip on;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
    include /etc/nginx/mime.types;

    location /ws/ {
        proxy_pass http://websocket;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $server_name;
    }

    location / {
        uwsgi_pass  escriptorium;
        include     uwsgi_params;
        uwsgi_param HTTP_X_FORWARDED_PROTO $scheme;

        proxy_pass http://escriptorium;
        proxy_set_header X-Forwarded-Proto "https";
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
    }

{% if escriptorium.data is defined and escriptorium.data.nfs.local_driver %}
    location /static/ {
        alias {{ escriptorium.data.dir }}/static/;
    }

    location /media/ {
        alias {{ escriptorium.data.dir }}/media/;
    }

    location = /robots.txt {
        alias {{ escriptorium.data.dir }}/robots.txt;
    }
{% else %}
    location /static/ {
        alias /usr/src/app/static/;
    }

    location /media/ {
        alias /usr/src/app/media/;
    }

    location = /robots.txt {
        alias /usr/src/app/static/robots.txt;
    }
{% endif %}
}

