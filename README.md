# eScriptorium deployment

Deployment scripts for the eScriptorium app (https://gitlab.com/scripta/escriptorium) using [Ansible](https://docs.ansible.com/).

For **development and local instances** (to test the software), you can use the simpler [docker-compose.yml](https://gitlab.com/scripta/escriptorium/-/blob/develop/docker-compose.yml) from the eScriptorium repository.

For **production** deployment, this repository should be favored, at it takes into account monitoring, SSL certificates, backup support, etc...

## Instances managed with this code

- https://escriptorium.fr
- https://escriptorium.openiti.org

## Setup for operators

Any operator that want to use this code for their own eScriptorium instance should have 2 Git repositories on their computer:
- a local clone of this repository, to get a generic deployment playbook
- a private repository that will hold extra Ansible roles and private configuration (Database password, connectivity to servers, ...)


To setup the local clone of this repository and make it available to your own playbook, simply do the following:

```console
git clone git@gitlab.com:scripta/escriptorium-deployment.git
ln -s $(pwd)/escriptorium-deployment/roles/escriptorium ~/.ansible/roles/
```

You can create a new Ansible playbook with the following configuration:

```yaml
- hosts:
    - my-server

  roles:
    - role: escriptorium
      tags: escriptorium
```

This will run the `escriptorium` role from **this repository** on your server.

## Components

This escriptorium Ansible role combines multiple components:
- (optional) setup for Docker to run the different containers: You can manage the docker setup on your own, but **Docker is required** on the server. 
- (optional) setup for [Traefik](https://doc.traefik.io/) to expose the web application publicly and manage the SSL certificate
- setup of the eScriptorium Docker containers
- (optional) setup for [Flower](https://flower.readthedocs.io/en/latest/) to manage the Celery tasks created by eScriptorium

## Architecture

```mermaid
graph TD
    users --> traefik{Traefik}
    traefik --> nginx
    nginx --> static[Static files]
    nginx --> backend[eScriptorium app]
    nginx --> daphne[eScriptorium websockets]
    backend --> db
    daphne --> db
    backend --> redis
    backend --> mail[Mail server]
    daphne --> mail
    worker_main[Async worker main] --> db
    worker_live[Async worker live] --> db
    worker_gpu[Async worker GPU] --> db
    redis --> worker_main
    redis --> worker_live
    redis --> worker_gpu
```

## Host configuration

You can view all available configuration in the commented file [host_vars.sample.yml](host_vars.sample.yml) from this repository.

It's a good starting base when using the escriptorium role on an host: you can use it as `host_vars/my-server` in your own configuration project.

## Encryption

The Ansible deployment code needs to reference soem values that should be encrypted, even on private repositories (such as sensitive passwords or tokens).

[Ansible provides a way](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_vault.html) to use and store encrypted strings set in an otherwise non-encrypted file. This is particularly useful for our use-case, as only a few variables need to be encrypted: developers can easily update an unencrypted text file, and see that some parts are encrypted.

You can identify the encrypted variable by their YAML prefix `!vault`.

As common variables are encrypted (see below), we always provide the decryption password to ansible when using it (through `--vault-id=./password.sh`). The script `password.sh` must be a locally available file in your own configuration repo that will echo the decryption password on standard output. You could use a GPG encrypted file for example, and decrypt it with your own private key.

Configuration variables that should be encrypted:
- `escriptorium.db_root_password`
- `escriptorium.django_secret_key`
- `escriptorium.django_admin_password`

### New passhphrase

Here is a Makefile command to generate a random passphrase with its encrypted version so that users can easily copy/paste it in your own configuration repository:

```
passphrase:
	$(eval pass := $(shell tr -cd '[:alnum:]' < /dev/urandom | fold -w42 | head -n1 | tr -d '\n'))
	@echo "Passphrase: $(pass)"
	echo -n $(pass) | ansible-vault encrypt_string --vault-id=./password
```
